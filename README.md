## SOAL 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi :

### Bagian 1, 2, 3, 4

#### Deskripsi Problem

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

#### Solusi

Bocchi memfilter dengan mengunakan kata kunci `Japan` dan mengurutkan data dari yang terbesar ke terkecil berdasarkan kolom `rank`. Kemudian, Bocchi memilih 5 data teratas dari hasil filter tersebut. Membuat sebuah script bash yang terdiri dari beberapa perintah awk dan sort untuk memanipulasi data dari file rankUNIV.csv dan mengeluarkan hasilnya pada terminal.

#### Code dan Pejelasan


1. echo "soal1" : Perintah ini hanya menampilkan teks "soal1" di terminal sebagai penanda bahwa kita sedang menjalankan perintah untuk soal nomor 1.
awk -F "," '/Japan/ {print}' rankUNIV.csv : Perintah ini akan membaca isi file rankUNIV.csv dan mencari baris yang mengandung kata "Japan". Kemudian perintah print akan mencetak baris tersebut ke terminal.
head -n 5 : Perintah ini akan membatasi output dari perintah awk sebelumnya hanya untuk 5 baris pertama saja. Kemudian hasilnya akan disimpan ke dalam file hasilsortir.csv.
awk -F "," '{print $1, $2}' hasilsortir.csv : Perintah ini akan membaca file hasilsortir.csv dan mencetak kolom pertama dan kedua dari setiap baris ke terminal, dipisahkan oleh spasi.

2. echo "soal2" : Perintah ini hanya menampilkan teks "soal2" di terminal sebagai penanda bahwa kita sedang menjalankan perintah untuk soal nomor 2.
awk -F "," '{print $9, $2}' hasilsortir.csv | sort : Perintah ini akan membaca file hasilsortir.csv dan mencetak kolom kesembilan dan kedua dari setiap baris ke terminal, dipisahkan oleh spasi. Kemudian hasilnya akan diurutkan secara ascending berdasarkan kolom pertama menggunakan perintah sort.

3. echo "soal3" : Perintah ini hanya menampilkan teks "soal3" di terminal sebagai penanda bahwa kita sedang menjalankan perintah untuk soal nomor 3.
awk -F "," '/Japan/ {print $20, $2}' rankUNIV.csv | sort -n | head -10 : Perintah ini akan membaca file rankUNIV.csv dan mencetak kolom kedua puluh dan kedua dari setiap baris yang mengandung kata "Japan" ke terminal, dipisahkan oleh spasi. Kemudian hasilnya akan diurutkan secara numerik secara ascending berdasarkan kolom pertama menggunakan perintah sort, dan hanya diambil 10 baris pertama saja menggunakan perintah head.

4. echo "soal4" : Perintah ini hanya menampilkan teks "soal4" di terminal sebagai penanda bahwa kita sedang menjalankan perintah untuk soal nomor 4.
awk -F "," '/Keren/ {print}' rankUNIV.csv : Perintah ini akan membaca file rankUNIV.csv dan mencetak setiap baris yang mengandung kata "Keren" ke terminal.



## SOAL 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

## Name
Choose a self-explaining name for your project.
# DESKRIPSI SOAL
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.
## Solusi soal 2a
Pertama, untuk menyelesaikan tugas kita harus mengecek apakah folder kumpulan sudah ada atau belum, dibuat jika sudah dibuat maka kita harus mencari tau ada berapa folder yang sudah dibuat.

```R
lastfolder=1
while [ -d kumpulan_$lastfolder ]
  do
    lastfolder=$((lastfolder+1))
  done
  ```
Selanjutnya, kita buat folder berdasarkan variabel lastfoldernya.
```R
mkdir "kumpulan_$lastfolder"
```
Jika sudah, kita akan maka membuat sebuah kondisi sesuai dengan soal apabila jam menunjukkan pukul 00.00 maka download 1 gambar saja.
```R
  hour=$(date +"%H")
  if [ $hour -eq "00" ]
  then 
    hour=1
  fi
  ```
Kemudian, kita akan mengunduh gambar berdasarkan link yang sudah kita sediakan ke file yang banyaknya sesuai dengan jam ketika mengunduh file tersebut dan meletakannya kedalam folder kumpulan. 
```R
num=1
  while [ $num -le $hour ]
  do
    wget -O "kumpulan_$lastfolder"/"pemandangan_$num.jpg" https://dagodreampark.co.id/media/k2/items/cache/be4e4fd1bcb87d92f342f6e3e3e1d9e2_XL.jpg
    num=$((num+1))
  done
  ```
  Dari script tersebut, kita akan mengunduh image dengan penamaan pemandangan_$num dan $num adalah urutan dari image yang di unduh. 

Seblum script tersebut kita jalankan dengan cron job, kode diatas harus dibungkus dengan fungsi yang bernama soal2a dan cara aksesnya adalah bash kobeni_liburan.sh 1 supaya bisa kita aplikasikan bersamaan dengan soal2b.

**CRON JOB**
```R
0 */10 * * * /home/ubuntu/Downloads/kobeni_liburan.sh 1
```
Cron job diatas berfungsi untuk menjalankan kode dengan rentang waktu setiap 10 jam.

## Solusi 2b
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

 Pertama mendeklarasikan variabel loop lalu kita akan mengecek apakah folder devil_$num.zip sudah ada atau belum, jika sudah ada kita akan mencari tau berapa folder yang dibuat.

 ```R
 num=1
 	while [ -d kumpulan_$num  ]
 	do 
	if [ -f devil_$num.zip ]
	then
	num=$((num+1))
	continue
    ``` 
 kemudian kita akan melakukan zip kepada folder tersebut dengan penamaan devil_$num.zip 
 ```R
 fi
	zip -r "devil_$num.zip" "kumpulan_$num"
	num=$((num+1))

	done 
    ```

Selanjutnya terdapat command untuk menjalankan soal2a dan soal2b.
```R
f [ ! $1  ]
then
  echo "Enter number 1 or 2 at the end of the command"
  exit
fi

if [ $1 -eq 1 ]
then
  soal2a

elif [ $1 -eq 2 ]
then
  soal2b
fi
```
dari script diatas kita harus memilih untuk menjalankan soal 2a atau soal 2b dengan menginput angka 1 atau 2.

## SOAL 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

### Point pertama

#### Deskripsi 

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut

1. Minimal `8 karakter`
2. Memiliki minimal 1 `huruf kapital` dan 1 `huruf kecil`
3. `Alphanumeric`
4. Tidak boleh sama dengan `username`
5. Tidak boleh menggunakan kata `chicken` atau `ernie`

#### Solusi

Kode yang diberikan adalah sebuah program Bash yang meminta pengguna untuk memasukkan ID dan password, kemudian melakukan serangkaian pemeriksaan terhadap password untuk memastikan bahwa password tersebut memenuhi kriteria keamanan tertentu sebelum menyimpannya dalam file "users.txt" dan menuliskan informasi ke log file "log.txt". Berikut ini penjelasan detail dari setiap baris kode:

#### Code

kode louis.sh :

```bash
echo "sign in"
read -p 'ID : ' ID
read -p 'Password : ' pass
passlen=`expr "$pass" : '.*'`
passup=${pass^^}
passlow=${pass,,}
if [ $passlen -lt 8 ];
then
	echo "Password kurang dari 8"
else
	if ! [[ "$pass" =~ [^0-9] ]];
	then
		echo "Kombinasi Huruf dan Angka"
	else
		if [ $pass == $passup ];
		then
			echo "minimal 1 lowercase"
		elif [ $pass == $passlow ];
		then
			echo "minimal 1 uppercase"
		else
			if [[ "$pass" =~ [^a-zA-Z0-9] ]];
			then
				echo "Bukan Alphanumeric"
			else
				if ! [[ "$pass" =~ [^a-zA-Z] ]];
				then
					echo "Kombinasi Huruf dan Angka"
				else
					if [ $pass == $ID ];
					then
						echo "Pass dilarang sama dengan ID"
					else
						if [[ $pass =~ "chicken" ]];
						then
							echo "tidak boleh 'chicken'"
						elif [[ $pass =~ "ernie" ]];
						then
							echo "tidak boleh 'ernie'"
						else
							while IFS=, read -r u p;
							do
								if [[ "$u" == "$username" ]];
								then
									echo "$(date +'%y/%m/%d %T') REGISTER: ERROR User already exists" >> log.txt
									exit 0
								fi
							done < ./users/users.txt
							echo "$ID,$pass" >> ./users/users.txt
							echo "$(date +'%y/%m/%d %T') REGISTER: INFO User $ID registered successfully" >> log.txt
						fi
					fi
				fi
			fi
		fi
	fi
fi


```

**Penjelasan dan tahapan** 

1. Pada awalnya, program akan mencetak "sign in" ke layar dan meminta pengguna memasukkan ID dan password. ID dan password yang dimasukkan oleh pengguna akan disimpan dalam variabel ID dan pass, masing-masing.

2. program akan menghitung panjang password yang dimasukkan oleh pengguna dengan menggunakan perintah expr dan menyimpannya dalam variabel passlen. Program juga akan membuat dua salinan dari password yang dimasukkan, yaitu passup yang berisi salinan uppercase dari password dan passlow yang berisi salinan lowercase dari password.

3. Program akan melakukan validasi password yang dimasukkan oleh pengguna, dimulai dengan memeriksa apakah panjang password kurang dari 8 karakter. Jika ya, program akan mencetak "Password kurang dari 8" ke layar. Jika tidak, program akan melanjutkan dengan validasi selanjutnya.

4. Validasi selanjutnya adalah memeriksa apakah password terdiri dari kombinasi huruf dan angka. Jika password hanya terdiri dari angka saja, program akan mencetak "Kombinasi Huruf dan Angka" ke layar. Jika tidak, program akan melanjutkan dengan validasi selanjutnya.

5. Validasi selanjutnya adalah memeriksa apakah password memiliki setidaknya satu karakter uppercase dan satu karakter lowercase. Jika password hanya terdiri dari karakter uppercase, program akan mencetak "minimal 1 lowercase" ke layar. Jika password hanya terdiri dari karakter lowercase, program akan mencetak "minimal 1 uppercase" ke layar. Jika tidak, program akan melanjutkan dengan validasi selanjutnya.

6. Validasi selanjutnya adalah memeriksa apakah password terdiri dari karakter alphanumeric. Jika password mengandung karakter selain huruf dan angka, program akan mencetak "Bukan Alphanumeric" ke layar. Jika tidak, program akan melanjutkan dengan validasi selanjutnya.

7. Validasi selanjutnya adalah memeriksa apakah password sama dengan ID yang dimasukkan oleh pengguna. Jika password sama dengan ID, program akan mencetak "Pass dilarang sama dengan ID" ke layar. Jika tidak, program akan melanjutkan dengan validasi selanjutnya.

8. Validasi selanjutnya adalah memeriksa apakah password mengandung kata "chicken" atau "ernie". Jika password mengandung kata tersebut, program akan mencetak "tidak boleh 'chicken'" atau "tidak boleh 'ernie'" ke layar. Jika tidak, program akan melanjutkan dengan validasi selanjutnya.

9. Validasi selanjutnya adalah memeriksa apakah ID yang dimasukkan oleh pengguna sudah terdaftar di file ./users/users.txt. Jika ID sudah terdaftar, program akan mencetak pesan kesalahan "User already exists" dan menambahkan entri ke file log.txt dengan status error. Jika ID belum terdaftar, program akan menambahkan entri ke file ./users/users.txt dengan format "ID,password" dan menambahkan entri ke file log.txt dengan status info.


### Point kedua

### Deskripsi 

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists

2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully

3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME

4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

### Solusi

Sebuah program bash shell yang meminta pengguna untuk memasukkan ID dan kata sandi (password) untuk login. Setelah itu, program tersebut membaca file teks yang berisi daftar ID dan kata sandi yang sah. Jika ID dan kata sandi yang dimasukkan pengguna cocok dengan salah satu pasangan ID dan kata sandi dalam file tersebut, program akan mencetak pesan yang menunjukkan bahwa pengguna telah berhasil masuk dan menambahkan entri log yang mencatat waktu masuk pengguna. Jika tidak ada pasangan ID dan kata sandi yang cocok dalam file tersebut, program akan mencetak pesan yang menunjukkan bahwa upaya masuk pengguna gagal dan menambahkan entri log yang mencatat upaya masuk yang gagal.

### Code
kode retep.sh :


```bash
echo "login"
read -p "ID : " ID
read -p "Password : " pass

while IFS=, read -r u p;
do
	if [[ "$u" == "$ID" && "$p" == "$pass" ]];
	then
		echo "$(date +'%y/%m/%d %T') LOGIN: INFO User $ID logged in" >> log.txt
		exit 0
	fi
done < ./users/users.txt

echo "$(date +'%y/%m/%d %T') LOGIN: ERROR Failed login attempt on user $ID" >> log.txt
```



**Penjelasan** 

1. echo "login" : Mencetak kata "login" pada terminal sebagai tanda bahwa program dimulai.
2. read -p "ID : " ID : Meminta pengguna memasukkan ID dengan menampilkan pesan "ID : " dan menyimpan input pengguna pada variabel ID.
3. read -p "Password : " pass : Meminta pengguna memasukkan kata sandi (password) dengan menampilkan pesan "Password : " dan menyimpan input pengguna pada variabel pass.
4. while IFS=, read -r u p; : Melakukan loop pada setiap baris dalam file users.txt yang dipisahkan oleh tanda koma (,) dan menyimpan nilai dari kolom pertama pada variabel u dan nilai dari kolom kedua pada variabel p.
5. if [[ "$u" == "$ID" && "$p" == "$pass" ]]; : Memeriksa apakah u sama dengan ID dan p sama dengan pass.
6. then : Jika pernyataan pada baris 5 benar (true), jalankan perintah-perintah pada baris 7-8.
7. echo "$(date +'%y/%m/%d %T') LOGIN: INFO User $ID logged in" >> log.txt : Mencetak pesan yang menunjukkan bahwa pengguna telah berhasil masuk dan menambahkan entri log yang mencatat waktu masuk pengguna ke file log.txt.
8. exit 0 : Menghentikan program dengan kode keluaran 0 (exit code), menunjukkan bahwa program telah berakhir tanpa kesalahan.
9. fi : Menandai akhir blok kondisional yang dimulai pada baris 5.
10. done < ./users/users.txt : Menandakan akhir dari loop pada baris 4 dan mengambil data dari file users.txt yang terletak pada direktori users.
11. echo "$(date +'%y/%m/%d %T') LOGIN: ERROR Failed login attempt on user $ID" >> log.txt : Mencetak pesan yang menunjukkan bahwa upaya masuk pengguna gagal dan menambahkan entri log yang mencatat upaya masuk yang gagal ke file log.txt.


## Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:

    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a


-Buat juga script untuk dekripsinya.
-Backup file syslog setiap 2 jam untuk dikumpulkan
 
## Encrypt


```bash
arahLog="/var/log/syslog
```
Befungsi untuk mengset SYSLOG path di arahLog variabel


```bash
encryptedArtifacts="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
``` 
Merupakan alphabets yang akan dienkripsi dan di cipher

```bash
batasAtas="$(echo ${encryptedArtifacts:26:51})"
batasBawah="$(echo ${encryptedArtifacts:0:26})"
```

Befungsi untuk memisahkan huruf besar dan huruf kecil yang sudah dimasukan pada encryptedArtifacts

```bash
temp+="${batasBawah:agentOfEncryption%26:26}${batasBawah:0:agentOfEncryption%26}"
    temp+="${batasAtas:agentOfEncryption%26:26}${batasAtas:0:agentOfEncryption%26}"
    echo $temp
``` 
Befungsi untuk mendapatkan range dari setiap kasus menggunakan konsep substring





```bash
function ThyFullEncryptionPack () {
    local filename="$(date +"%H:%M %d:%m:%Y").txt"
    echo -n "$(cat $arahLog | tr [$(echo $encryptedArtifacts)] [$(encrypt $(date +"%H"))] >> "$filename")"
```
Berfungsi untuk mengimplementasi fungsi enkripsi di arahlog inner text




```bash
echo -n "$(ThyFullEncryptionPack)"
```
mengenkripsi secara full syslog dan mencadangkan data ke txt fileF




# Decrypt

```bash
filePath="$(ls | awk '/.txt/' | tail -n 1)"
getHour=$(ls | awk '/.txt/' | sort -t: -k1 | tail -n 1 | cut -d':' -f1)
```
Untuk mengset file ke recent log dan mengambil jamnya 

```bash
function ThyFullDecryptionPack () {
    echo -n "$(cat "$filePath" | tr [$(decrypt $getHour)] [$(echo $decryptedArtifacts)])"
```
Untuk mengimplementasi fungsi dekripsi 

```bash
echo -n "$(ThyFullDecryptionPack)"
```
Berfungsi untuk memanggil decrypt pack
